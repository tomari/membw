#include <stdlib.h>
#include "membw.h"
#ifdef _OPENMP
#include <omp.h>
#endif

#ifndef PREFETCH
#define PREFETCH 256
#endif

#ifdef __AVX__
#include <immintrin.h>
FILL_REGION(x86avx) {
	unsigned l;
	size_t i;
	unsigned long long *u=(void*)&v;
	__m256i pat256=_mm256_set1_epi64x(*u);
	for(l=0; l<nreps; l++) {
#pragma omp parallel for
		for(i=0; i<nw; i+=16) {
			_mm256_stream_si256((__m256i*)&r[i],pat256);
			_mm256_stream_si256((__m256i*)&r[i+4],pat256);
			_mm256_stream_si256((__m256i*)&r[i+8],pat256);
			_mm256_stream_si256((__m256i*)&r[i+12],pat256);
		}
	}
	_mm_empty();
}
READ_REGION(x86avx) {
	unsigned l;
	size_t i;
	for(l=0; l<nreps; l++) {
#pragma omp parallel for
		for(i=0; i<nw; i+=16) {
			volatile register __m256i x,y,z,w;
			x=_mm256_load_si256((__m256i*)&r[i]);
			_mm_prefetch((char*)&r[i+PREFETCH],_MM_HINT_NTA);
			y=_mm256_load_si256((__m256i*)&r[i+4]);
			_mm_prefetch((char*)&r[i+4+PREFETCH],_MM_HINT_NTA);
			z=_mm256_load_si256((__m256i*)&r[i+8]);
			_mm_prefetch((char*)&r[i+8+PREFETCH],_MM_HINT_NTA);
			w=_mm256_load_si256((__m256i*)&r[i+12]);
			_mm_prefetch((char*)&r[i+12+PREFETCH],_MM_HINT_NTA);
		}
	}
	_mm_empty();
}
MEMBW_KERNEL(x86avx);
#else 
EMPTY_KERN(x86avx);
#endif
