#include <stdlib.h>
#include "membw.h"
#ifndef PREFETCH
#define PREFETCH 24
#endif
#ifdef __SUNPRO_C
#include <sun_prefetch.h>
FILL_REGION(sun) {
	unsigned l;
	size_t i;
	for(l=0; l<nreps; l++) {
		for(i=0; i<nw; i+=4) {
			sun_prefetch_write_once(&r[i+PREFETCH]);
			r[i]=v;
			r[i+1]=v;
			sun_prefetch_write_once(&r[i+2+PREFETCH]);
			r[i+2]=v;
			r[i+3]=v;
		}
	}
}
READ_REGION(sun) {
	unsigned l;
	size_t i;
	for(l=0; l<nreps; l++) {
		for(i=0; i<nw; i+=4) {
			volatile register double x,y,z,w;
			sun_prefetch_read_once(&r[i+PREFETCH]);
			x=r[i];
			y=r[i+1];
			sun_prefetch_read_once(&r[i+2+PREFETCH]);
			z=r[i+2];
			w=r[i+3];
		}
	}
}
MEMBW_KERNEL(sun);
#else
EMPTY_KERN(sun);
#endif
