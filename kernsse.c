#include <stdlib.h>
#include "membw.h"

#ifndef PREFETCH
#define PREFETCH 256
#endif

#ifdef __SSE__
#include <xmmintrin.h>
FILL_REGION(x86sse) {
	unsigned l;
	size_t i;
	union {
		double d;
		__m64 m;
	} pat64;
	pat64.d=v;
	for(l=0; l<nreps; l++) {
		for(i=0; i<nw; i+=4) {
			_mm_stream_pi((__m64*)&r[i],pat64.m);
			_mm_stream_pi((__m64*)&r[i+1],pat64.m);
			_mm_stream_pi((__m64*)&r[i+2],pat64.m);
			_mm_stream_pi((__m64*)&r[i+3],pat64.m);

		}
	}
	_mm_empty();		
}
READ_REGION(x86sse) {
	unsigned l;
	size_t i;
	for(l=0; l<nreps; l++) {
		for(i=0; i<nw; i+=4) {
			volatile register double x,y,z,w;
			x=r[i];
			y=r[i+1];
			_mm_prefetch((char*)&r[i+PREFETCH],_MM_HINT_NTA);
			z=r[i+2];
			w=r[i+3];
			_mm_prefetch((char*)&r[i+2+PREFETCH],_MM_HINT_NTA);
		}
	}
	_mm_empty();
}
MEMBW_KERNEL(x86sse);
#else
EMPTY_KERN(x86sse);
#endif
