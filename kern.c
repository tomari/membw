#ifndef KERN_RECURSIVE
#define KERN_RECURSIVE
#include <stdlib.h>
#include <stdio.h>
#include "membw.h"
#define MEMBW_KERN_REGISTER(name) extern struct membw_kern MEMBW_KERN_NAME(name);
#include "kern.c"
#undef MEMBW_KERN_REGISTER
#define MEMBW_KERN_REGISTER(name) &MEMBW_KERN_NAME(name),
const struct membw_kern *membw_kerns[] = {
#include "kern.c"
	NULL
};
#else /* KERN_RECURSIVE */
	MEMBW_KERN_REGISTER(dumb)
	MEMBW_KERN_REGISTER(x86avx)
	MEMBW_KERN_REGISTER(x86sse2)
	MEMBW_KERN_REGISTER(x86sse)
	MEMBW_KERN_REGISTER(ppc)
	MEMBW_KERN_REGISTER(sun)
	MEMBW_KERN_REGISTER(gcc)
	MEMBW_KERN_REGISTER(pwr7)
#endif
