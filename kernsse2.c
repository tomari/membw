#include <stdlib.h>
#include "membw.h"
#ifdef _OPENMP
#include <omp.h>
#endif

#ifndef PREFETCH
#define PREFETCH 256
#endif

#ifdef __SSE2__
#include <emmintrin.h>
FILL_REGION(x86sse2) {
	__m128d pat128;
	unsigned l;
	size_t i;
	pat128=_mm_set1_pd(v);
	for(l=0; l<nreps; l++) {
#pragma omp parallel for
		for(i=0; i<nw; i+=8) {
			_mm_stream_pd(&r[i],pat128);
			_mm_stream_pd(&r[i+2],pat128);
			_mm_stream_pd(&r[i+4],pat128);
			_mm_stream_pd(&r[i+6],pat128);
		}
	}
	_mm_empty();
}
READ_REGION(x86sse2) {
	unsigned l;
	size_t i;
	for(l=0; l<nreps; l++) {
#pragma omp parallel for
		for(i=0; i<nw; i+=8) {
			volatile register __m128i x,y,z,w;
			x=_mm_load_si128((__m128i*)&r[i]);
			_mm_prefetch((char*)&r[i+PREFETCH],_MM_HINT_NTA);
			y=_mm_load_si128((__m128i*)&r[i+2]);
			_mm_prefetch((char*)&r[i+2+PREFETCH],_MM_HINT_NTA);
			z=_mm_load_si128((__m128i*)&r[i+4]);
			_mm_prefetch((char*)&r[i+4+PREFETCH],_MM_HINT_NTA);
			w=_mm_load_si128((__m128i*)&r[i+8]);
			_mm_prefetch((char*)&r[i+8+PREFETCH],_MM_HINT_NTA);
		}
	}
	_mm_empty();
}
MEMBW_KERNEL(x86sse2);
#else
EMPTY_KERN(x86sse2);
#endif
