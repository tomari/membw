## Build configuration
#USE_GNUTLS = 1 # Uncomment to use GnuTLS for generating random numbers
#USE_OPENSSL = 1 # Uncomment to use OpenSSL for generating random numbers

## Optimization flags
CFLAGS ?= -O3 -march=native -fopenmp
LDFLAGS ?= $(CFLAGS)


ifdef USE_GNUTLS
	CFLAGS += -DUSE_GNUTLS=1
	LDFLAGS += -lgnutls
else ifdef USE_OPENSSL
	CFLAGS += -DUSE_OPENSSL=1
	LDFLAGS += -lcrypto
endif

membw2: membw2.o kernsse2.o kernsse.o kernavx.o kern.o dtime.o kernppc.o kernsun.o kerngcc.o kernpwr7.o

.PHONY: clean
clean:
	rm -f *.o membw2

