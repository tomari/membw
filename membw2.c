/* Memory bandwidth testing program -- Hisanobu Tomari */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#ifdef _OPENMP
#include <omp.h>
#endif

#ifdef USE_GNUTLS
#include <gnutls/gnutls.h>
#include <gnutls/crypto.h>
#elif defined(USE_OPENSSL)
#include <openssl/rand.h>
#endif

#include "membw.h"

static const unsigned long align=4096; /* must be power of two */
/* timers*/
static void start(void);
static double end(void);
/* kern mgmt */
static void list_kern(void);
static const struct membw_kern *search_kern(const char *name);
/* etc */
static int run(const struct membw_kern *k, size_t nb, unsigned nloops);
/* random */
static void fill_random_number(unsigned char *address, long length);

extern int main(int argc, char *argv[]);

extern int main(int argc, char *argv[]) {
	const struct membw_kern *k;
	size_t nb;
	unsigned l=1;
	if(argc<3) {
		list_kern();
		return EXIT_FAILURE;
	}
	nb=1024LL*1024LL*strtol(argv[1],NULL,10);
	if(!(k=search_kern(argv[2]))) {
		list_kern();
		return EXIT_FAILURE;
	}
	if(argc>3)
		l=strtol(argv[3],NULL,10);
	return run(k,nb,l);
}

/* timer */
static double starttime;
static void start()
{
	starttime=dtime();
}

static double end()
{
	double endtime=dtime();
	return endtime-starttime;
}

/* random number */
static void fill_random_number(unsigned char *address, long length) {
#ifdef USE_GNUTLS
	if(gnutls_rnd(GNUTLS_RND_NONCE, (void*)address, length)<0) {
		fputs("gnutls_rnd returned negative\n",stderr);
	}
#elif defined(USE_OPENSSL)
	if(RAND_pseudo_bytes(address,(int)length)<0) {
		fputs("RAND_pseudo_bytes not supported in OpenSSL\n",stderr);
	}
#else
	long i;
	for(i=0L; i<length; i++)
		address[i]=(unsigned char)rand();
#endif
}

/* run */
static int run(const struct membw_kern *k, size_t nb, unsigned nloops) {
	void *uchunk;	/* unaligned */
	double *chunk;	/* aligned */
	unsigned long i;
	double t,speed;
	int res=EXIT_SUCCESS;
	union {
		double d;
		uint64 i;
		unsigned char c[8];
	} pat;
#ifdef _OPENMP
	printf("compiled with OpenMP support. #th=%d\n",omp_get_max_threads());
#endif
	printf("Allocate %ld bytes...",nb); fflush(stdout);
	if(!(uchunk=malloc(nb+align))) {
		puts("failed");
		return EXIT_FAILURE;
	}
	chunk=(double*)(((unsigned long)uchunk+(align-1))&~(align-1));
	puts("ok");
	
	fputs("clearing ...",stdout); fflush(stdout);
	for(i=0; i<nb/8; i++)
		chunk[i]=0.;
	puts("ok");

	fill_random_number(&pat.c[0],sizeof(pat.c));
	printf("filling with pattern %02X%02X%02X%02X %02X%02X%02X%02X...",
	       pat.c[7],pat.c[6],pat.c[5],pat.c[4],
	       pat.c[3],pat.c[2],pat.c[1],pat.c[0]); fflush(stdout);
	start();
	k->fill_region(chunk,nb/8,pat.d,nloops);
	t=end();
	speed=((double)nloops)*((double)nb)/t/1024./1024.; /* MB/s */
	printf("ok %f sec = %f MB/s\n",t,speed);
	
	fputs("verifying...",stdout); fflush(stdout);
	for(i=0; i<nb/8; i++) {
		uint64 rr=((uint64 *) chunk)[i];
		if(rr!=pat.i) {
			pat.i=rr;
			printf("got %02X%02X%02X%02X %02X%02X%02X%02X @ %ld ",
			       pat.c[7],pat.c[6],pat.c[5],pat.c[4],
			       pat.c[3],pat.c[2],pat.c[1],pat.c[0],i);
			res=EXIT_FAILURE;
		}
	}
	puts("ok");
	
	fputs("reading...",stdout); fflush(stdout);
	start();
	k->read_region(chunk,nb/8,nloops);
	t=end();
	speed=((double)nloops)*((double)nb)/t/1024./1024.; /* MB/s */
	printf("ok %f sec = %f MB/s\n",t,speed);
	return res;
}

/* kernel list mgmt */
static void list_kern() {
	unsigned i=0;
	const struct membw_kern *k;
	puts("usage: membw MBytes kern\n  where kern is one of:");
	while((k=membw_kerns[i++])) {
		if(k->name)
			puts(k->name());
	}
}

static const struct membw_kern *search_kern(const char *name) {
	unsigned i=0;
	const struct membw_kern *k=NULL;
	while((k=membw_kerns[i++])) {
		if(k->name && !(strcmp(k->name(),name)))
			break;
	}
	return k;
}

/* dumb kernel */
FILL_REGION(dumb) {
	unsigned l;
	size_t i;
	for(l=0; l<nreps; l++) {
		for(i=0; i<nw; i+=4) {
			r[i]=v;
			r[i+1]=v;
			r[i+2]=v;
			r[i+3]=v;
		}
	}
}
READ_REGION(dumb) {
	unsigned l;
	size_t i;
	for(l=0; l<nreps; l++) {
		for(i=0; i<nw; i+=4) {
			volatile register double x,y,z,w;
			x=r[i];
			y=r[i+1];
			z=r[i+2];
			w=r[i+3];
		}
	}
}
MEMBW_KERNEL(dumb);
