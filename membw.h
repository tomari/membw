typedef unsigned int uint32;
typedef unsigned long long uint64;
struct membw_kern {
	void (*fill_region)(double *r, size_t nw, double v, uint32 nreps);
	void (*read_region)(double *r, size_t nw, uint32 nreps);
	char *(*name)(void);
	unsigned *(*unavail)(void);
};
extern double dtime(void);
extern const struct membw_kern *membw_kerns[];
#define FILL_REGION_NAME(name) fill_region_##name
#define FILL_REGION(name) static void FILL_REGION_NAME(name)(double *r, size_t nw, double v, uint32 nreps)
#define READ_REGION_NAME(name) read_region_##name
#define READ_REGION(name) static void READ_REGION_NAME(name)(double *r,size_t nw, uint32 nreps)

#define MEMBW_KERN_NAME(name) kern_##name
#define MEMBW_KERNEL(name) \
	static char *name_##name(){return #name;}; \
	struct membw_kern MEMBW_KERN_NAME(name) = { \
		FILL_REGION_NAME(name), \
		READ_REGION_NAME(name),	\
		name_##name \
	};
#define EMPTY_KERN(name) \
	struct membw_kern MEMBW_KERN_NAME(name) = { \
		NULL, \
		NULL, \
		NULL \
	};
