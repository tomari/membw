#include <stdlib.h>
#include "membw.h"
#ifndef PREFETCH
#define PREFETCH 24
#endif
#ifdef __GNUC__
FILL_REGION(gcc) {
	unsigned l;
	size_t i;
	for(l=0; l<nreps; l++) {
		for(i=0; i<nw; i+=4) {
			__builtin_prefetch(&r[i+PREFETCH],1,0);
			r[i]=v;
			r[i+1]=v;
			__builtin_prefetch(&r[i+2+PREFETCH],1,0);
			r[i+2]=v;
			r[i+3]=v;
		}
	}
}
READ_REGION(gcc) {
	unsigned l;
	size_t i;
	for(l=0; l<nreps; l++) {
		for(i=0; i<nw; i+=4) {
			volatile register double x,y,z,w;
			__builtin_prefetch(&r[i+PREFETCH],0,0);
			x=r[i];
			y=r[i+1];
			__builtin_prefetch(&r[i+2+PREFETCH],0,0);
			z=r[i+2];
			w=r[i+3];
		}
	}
}
MEMBW_KERNEL(gcc);
#else
EMPTY_KERN(gcc);
#endif
