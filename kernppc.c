#include <stdlib.h>
#include "membw.h"

#ifndef PREFETCH
#define PREFETCH 24
#endif
#ifdef __PPC__
#include <ppu_intrinsics.h>
FILL_REGION(ppc) {
	unsigned l;
	size_t i;
	for(l=0; l<nreps; l++) {
		for(i=0; i<nw; i+=4) {
			/* does not support cache line size > 32 bytes */
			__dcbz(&r[i]);
			r[i]=v;
			r[i+1]=v;
			r[i+2]=v;
			r[i+3]=v;
		}
	}
}
READ_REGION(ppc) {
	unsigned l;
	size_t i;
	for(l=0; l<nreps; l++) {
		for(i=0; i<nw; i+=4) {
			volatile register double x,y,z,w;
			x=r[i];
			y=r[i+1];
			__dcbt(&r[i+PREFETCH]);
			z=r[i+2];
			w=r[i+3];
			__dcbt(&r[i+2+PREFETCH]);
		}
	}
}
MEMBW_KERNEL(ppc);
#else
EMPTY_KERN(ppc);
#endif
