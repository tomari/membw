#include <stdlib.h>
#include "membw.h"
#ifdef _OPENMP
#include <omp.h>
#endif

#ifndef PREFETCH
#define PREFETCH 128
#endif
#ifdef __PPC__
#include <ppu_intrinsics.h>
FILL_REGION(pwr7) {
	unsigned l;
	size_t i;
	for(l=0; l<nreps; l++) {
#pragma omp parallel for
		for(i=0; i<nw; i+=16) {
			/* does not support cache line size > 32 bytes */
			__dcbz(&r[i]);
			r[i]=v;
			r[i+1]=v;
			r[i+2]=v;
			r[i+3]=v;
			r[i+4]=v;
			r[i+5]=v;
			r[i+6]=v;
			r[i+7]=v;
			r[i+8]=v;
			r[i+9]=v;
			r[i+10]=v;
			r[i+11]=v;
			r[i+12]=v;
			r[i+13]=v;
			r[i+14]=v;
			r[i+15]=v;
		}
	}
}
READ_REGION(pwr7) {
	unsigned l;
	size_t i;
	for(l=0; l<nreps; l++) {
#pragma omp parallel for
		for(i=0; i<nw; i+=16) {
			volatile register double x,y,z,w,s,t,u,v;
			volatile register double a,b,c,d,e,f,g,h;
			x=r[i];
			y=r[i+1];
			z=r[i+2];
			w=r[i+3];
			s=r[i+4];
			t=r[i+5];
			u=r[i+6];
			v=r[i+7];
			a=r[i+8];
			b=r[i+9];
			c=r[i+10];
			d=r[i+11];
			e=r[i+12];
			f=r[i+13];
			g=r[i+14];
			h=r[i+15];
			__dcbt(&r[i+16+PREFETCH]);
		}
	}
}
MEMBW_KERNEL(pwr7);
#else
EMPTY_KERN(pwr7);
#endif
